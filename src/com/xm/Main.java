package com.xm;

import com.xm.engine.MovementCalculator;
import com.xm.model.Board;
import com.xm.model.Knight;
import com.xm.model.Node;

public class Main {

    private static Board board = new Board();



    public static void main(String[] args) {
        Node source;
        Node dest;
        MovementCalculator calculator = new MovementCalculator();
        Knight knight = new Knight();

        System.out.println("Argument count: " + args.length);
        System.out.println("Source: " + args[0]);
        System.out.println("Destination: " + args[1]);

        source = (Node) board.getBoardMap().get(args[0]);
        dest = (Node) board.getBoardMap().get(args[1]);
        source.setName(args[0]);
        dest.setName(args[1]);

        calculator.setInitialNeighbors(knight.findNeighbors(source));
        calculator.findShortestPath(source, dest);

    }
}
