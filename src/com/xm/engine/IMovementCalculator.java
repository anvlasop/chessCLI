package com.xm.engine;

import com.xm.model.Node;

public interface IMovementCalculator {

    int findShortestPath(Node source, Node dest);
}
