package com.xm.engine;

// source -> https://www.baeldung.com/java-breadth-first-search

import com.xm.model.Knight;
import com.xm.model.Node;

import java.util.*;

public class MovementCalculator implements IMovementCalculator {

    private Knight knight = new Knight();
    private int steps = 0;
    int visitedNodeCounter = 0;
    private SortedSet<Node> initialNeighbors =  new TreeSet<>();

    public SortedSet<Node> getInitialNeighbors() {
        return initialNeighbors;
    }

    @Override
    public int findShortestPath(Node source, Node dest) {
        if (steps > 2) {
            steps = 0;
            initialNeighbors.remove(source);
            findShortestPath(initialNeighbors.first(), dest);
        } else {
            SortedSet<Node> neighbors = knight.findNeighbors(source);
            for (Node neighbor: neighbors) {
                if (neighbor.getName().equals(dest.getName()) && steps < 3) {
                    System.out.println("Path found! Number of steps = " + steps);
                    return 1;
                }
            }
            initialNeighbors.remove(initialNeighbors.first());
            if (!initialNeighbors.isEmpty()) {
                steps = 0;
                findShortestPath(initialNeighbors.first(), dest);
            } else {
                System.out.println("No valid path found.");
                return 0;
            }
            ++steps;
        }
        return 0;
    }

    public void setInitialNeighbors(SortedSet<Node> initialNeighbors) {
        this.initialNeighbors = initialNeighbors;
    }
}













