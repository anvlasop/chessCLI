package com.xm.model;

import java.util.Objects;

/**
 * This class represents a node on the chess board.
 * It contains fields which correspond to coordinates and names of the chess board,
 * and the neighbored nodes
 */

public class Node implements Comparable<Node>
{
    private Coordinate coordinate;
    private String name;

    protected Node(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    protected Node(Coordinate coordinate, String name) {
        this.coordinate = coordinate;
        this.name = name;
    }

    protected Coordinate getCoordinate() {
        return coordinate;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public int compareTo(Node obj) {
        if (!(obj instanceof Node))
            return -1;
        return this.getName().compareToIgnoreCase(obj.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(coordinate, node.coordinate) &&
                Objects.equals(name, node.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinate, name);
    }
}
