package com.xm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Board {
    private List<Node> nodes;
    private HashMap<String, Node> boardMap = new HashMap();
    private List<Edge> edges;

    /**
     * This constructor creates a mapping between the position's names of board ("a1, b5..") and the nodes
     */
    public Board() {
        HashMap board = new HashMap();
        List<Node> nodeList = new ArrayList<>();
        int boardIndex = 0;
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                String boardPositionName = BOARD_POSITION_NAME.get(boardIndex++);
                nodeList.add(new Node(new Coordinate(x, y), boardPositionName));
                board.put(boardPositionName, new Node(new Coordinate(x, y), boardPositionName));
            }
        }
        this.nodes = nodeList;
        this.boardMap = board;
    }

    private static final List<String> BOARD_POSITION_NAME = Arrays.asList(
            "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",
            "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
            "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
            "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
            "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
            "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
            "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
            "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"
    );

    protected List<Node> getNodes() { return nodes; }

    public HashMap getBoardMap() { return boardMap; }

    public List<Edge> getEdges() { return edges; }
}
