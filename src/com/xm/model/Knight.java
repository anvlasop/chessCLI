package com.xm.model;

import java.util.*;

public class Knight {
    private Board board = new Board();

    private final List<Coordinate> possibleMoves = Arrays.asList(
            new Coordinate(2, -1),
            new Coordinate(2, 1),
            new Coordinate(-2, 1),
            new Coordinate(-2, -1),
            new Coordinate(1, 2),
            new Coordinate(1, -2),
            new Coordinate(-1, 2),
            new Coordinate(-1, -2)
    );


    public SortedSet<Node> findNeighbors(Node node) {
        SortedSet<Node> neighbors = new TreeSet<>();
        possibleMoves.forEach(possibleCoordinate -> {
            int aggregatedCoordX = node.getCoordinate().getX() + possibleCoordinate.getX();
            int aggregatedCoordY = node.getCoordinate().getY() + possibleCoordinate.getY();
            Coordinate potentialPosition = new Coordinate(aggregatedCoordX, aggregatedCoordY);
            if (isValid(potentialPosition)) {
                neighbors.add(new Node(potentialPosition,
                        findBoardName(new Node(new Coordinate(aggregatedCoordX, aggregatedCoordY)))
                ));
            }
        });
        return neighbors;
    }

    private String findBoardName(Node node) {
        return getKey(board.getBoardMap(), node.getCoordinate());
    }

    private boolean isValid(Coordinate pCoordinate) {
        return board.getNodes().stream().anyMatch(n -> n.getCoordinate().equals(pCoordinate));
    }

    private static String getKey(HashMap<String, Node> map, Coordinate value) {
        return map.entrySet()
                .stream()
                .filter(entry -> value.equals(entry.getValue().getCoordinate()))
                .map(Map.Entry::getKey)
                .findFirst().get();
    }
}
